<?php
try {
    if(!$argc) header("Content-type:text/plain; charset=utf-8");
    require_once("EAModelTools/autoloader.php");

    $model = new \EAModelTools\Model\Model(new EAModelTools\DBC\DBConnection("mysqlt://eap:eap@172.16.100.87/sparx_ea"));
    $export = new \EAModelTools\Model\ModelExport($model);

//    $export->exportModel("/Users/vatev/Documents/www/asd/model");
//    $export->exportPHP(dirname(__FILE__).'/generated_php');
//    $export->exportPHP("/Users/vatev/Documents/www/eol_km-2282/include/SOA");gfgfg

    $eePackage = null;
    foreach ($model->vcPackages as $package) if($package->nsURI == 'http://ee.econt.com/services') $eePackage = $package;


    //$export->exportPHP("/www/km-www/ee.econt.com/include/SOA");
    $export->exportModel("/www/km-www/ee.econt.com/services",$eePackage);
//    $export->exportModel("/Users/stavel/Documents/EnterpriseProjects/Model");

    echo "OK";

} catch (Exception $e) {
    $aMsg = array();
    $aMsg[] = $e->getMessage();
    $aMsg[] = $e->getFile()." line ".$e->getLine();
    $aMsg[] = "Trace:";
    foreach($e->getTrace() as $k => $aTraceRow) {
        $aMsg[] = "$k: {$aTraceRow['file']} {$aTraceRow['line']};";
    }
    echo (implode("\n",$aMsg));
}
