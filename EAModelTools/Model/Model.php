<?php
namespace EAModelTools\Model;

use EAModelTools\DBC\DBConnection;

class Model {
    public static function htmlEscape($txt) {
        return htmlentities($txt,ENT_QUOTES,'UTF-8');
    }

    public static function processNote($note) {
        return preg_replace_callback("/\\[\\[[^\\[\\]]+\\]\\]/",function ($matches){
            return $matches[0] == '[[br]]' ? '<br/>' : '<img src="'.trim($matches[0],'[]').'" />';
        }, $note);
    }

    /**
     * @var DBSmartConnection
     */
    private $dbc;

    /**
     * @var Package[]
     */
    public $packages;

    /** @var Package[] */
    public $vcPackages;

    /**
     * @var Type[]
     */
    public $types = array();

    /**
     * @var Type[][]
     */
    public $typesByName = array();

    public function __construct(DBConnection $dbc) {
        $this->dbc = $dbc;

        $this->load();
        foreach($this->packages as $package) if($package->nsURI) {
            if(!empty($package->types) && $package->getVCPackage()) $package->createSchema();
            $package->createHTML();
            foreach($package->diagrams as $diagram) $diagram->createHTML();
        }
        foreach($this->types as $type) if($type->type == "Interface" && !empty($type->operations) && $type->package->nsURI) $type->createWSDL();
    }

    private function load() {
        $this->packages = array();
        $packages = array(array('Package_ID' => 0));
        while(2) {
            $where = sprintf("p.Parent_ID IN (%s)", implode(',',array_map(function($r){return $r['Package_ID'];},$packages)));
            $packages = $this->dbc->select("
                SELECT
                    p.Package_ID,
                    p.Name,
                    p.Parent_ID,
                    o.Object_ID,
                    o.Note,
                    o.Stereotype,
                    op.Value as namespaceURI
                FROM t_package p
                LEFT JOIN t_object o ON o.PDATA1 = CONCAT(p.Package_ID,'') AND o.PDATA1 = p.Package_ID AND o.Object_Type = 'Package'
                LEFT JOIN t_objectproperties op ON op.Object_ID = o.Object_ID AND op.Property = 'namespaceURI'
                WHERE (o.Stereotype NOT IN ('NoCodeGen') OR o.Stereotype IS NULL)
                AND $where
            ");
            if(empty($packages)) break;
            foreach($packages as $packageRow) {
                $packageRow = array_map('trim',$packageRow);
                $packageIDs[] = $packageRow['Package_ID'];
                $this->packages[$packageRow['Package_ID']] = new Package($packageRow,$this->packages[$packageRow['Parent_ID']]);
                $this->packages[$packageRow['Package_ID']]->model = $this;
                if($this->packages[$packageRow['Parent_ID']]) $this->packages[$packageRow['Parent_ID']]->packages[] = $this->packages[$packageRow['Package_ID']];
            }
        }

        /** @var Package[] $packagesByTypeID */
        $packagesByTypeID = array();
        foreach($this->packages as $package) if($package->typeID) $packagesByTypeID[$package->typeID] = $package;
        if(!empty($this->packages)) foreach($this->dbc->select(sprintf("
            SELECT
                cn.Start_Object_ID,
                cn.End_Object_ID
            FROM t_connector cn
            WHERE cn.Connector_Type = 'Package'
            AND cn.Start_Object_ID IN (%s)
            "
            ,implode(',',array_keys($packagesByTypeID))
        )) as $packageImportRow) {
            $packagesByTypeID[$packageImportRow['Start_Object_ID']]->imports[] = $packagesByTypeID[$packageImportRow['End_Object_ID']];
        }

        foreach($this->packages as $package) {
            if($package->stereotype == 'VC') $this->vcPackages[] = $package;
            $importParentIDs = array();
            foreach($package->imports as $importPackage) {
                if($package->stereotype != 'VC') throw new \Exception("Non VC package {$package->name} ({$package->package->name}) can't import packages.");
                if($importPackage->stereotype != 'VC') throw new \Exception("{$package->name} ({$package->package->name}) can't import non VC package {$importPackage->name} ({$importPackage->package->name}).");
                if($importParentIDs[$importPackage->id]) throw new \Exception("{$package->name} ({$package->package->name}) imports multiple versions of {$importPackage->name} ({$importPackage->package->name}).");
                else $importParentIDs[$importPackage->id] = true;
            }
        }

        if(!empty($this->packages)) foreach($this->dbc->select(sprintf("
            SELECT
                o.Object_ID,
                o.Object_Type,
                o.Name,
                o.Note,
                o.Stereotype,
                o.Package_ID,
                tcg.End_Object_ID as parentTypeID
            FROM t_object o
            LEFT JOIN t_connector tcg ON tcg.Start_Object_ID = o.Object_ID AND tcg.Connector_Type = 'Generalization'
            WHERE o.Object_Type IN('Class','Enumeration','Interface')
            AND o.Package_ID IN (%s)
            ",implode(',',array_keys($this->packages)))) as $objectRow) {
            $objectRow = array_map('trim',$objectRow);
            $this->types[$objectRow['Object_ID']] = new Type($objectRow,$this->packages[$objectRow['Package_ID']]);
            $this->packages[$objectRow['Package_ID']]->types[] = $this->types[$objectRow['Object_ID']];
        }
        foreach($this->types as $type) $type->parentType = $this->types[$type->parentTypeID];
        foreach(Type::getBasicTypes() as $name => $type) $this->typesByName[$name][] = $type;
        foreach($this->types as $type) $this->typesByName[$type->name][] = $type;

        if(!empty($this->types)) {
            foreach($this->dbc->select(sprintf("
                SELECT
                    a.ID,
                    a.Object_ID,
                    a.Name,
                    a.Notes,
                    a.Stereotype,
                    COALESCE(a.LowerBound,'0') as LowerBound,
                    COALESCE(a.UpperBound, '1') as UpperBound,
                    a.Classifier,
                    a.Type
                FROM t_attribute a
                WHERE a.Object_ID IN (%s)
                ORDER BY Pos
            ",implode(',',array_keys($this->types)))) as $attributeRow) {
                $attributeRow = array_map('trim',$attributeRow);
                $ownerType = $this->types[$attributeRow['Object_ID']];

                if($attributeRow['Stereotype'] == 'enum') $attributeType = null;
                else $attributeType = $ownerType->getType($attributeRow['Type'],$attributeRow['Classifier']);

                $ownerType->attributes[] = new Attribute($attributeRow, $ownerType, $attributeType);
            }

            /** @var Operation[] $operations */
            $operations = array();
            foreach($this->dbc->select(sprintf("
                SELECT
                    op.OperationID,
                    op.Object_ID,
                    op.Name,
                    op.Notes,
                    op.Stereotype,
                    op.Classifier,
                    op.Type,
                    GROUP_CONCAT(ex_obj.Object_ID) as exception_object_ids
                FROM t_operation op
                LEFT JOIN t_xref ex_ref ON ex_ref.client = op.ea_guid AND ex_ref.Behavior = 'raisedException'
                LEFT JOIN t_object ex_obj ON ex_obj.ea_guid = ex_ref.Description
                WHERE op.Object_ID IN (%s)
                GROUP BY op.OperationID
                ORDER BY Pos
            ",implode(',',array_keys($this->types)))) as $operationRow) {
                $operationRow = array_map('trim',$operationRow);
                $ownerType = $this->types[$operationRow['Object_ID']];
                $operations[$operationRow['OperationID']] = new Operation(
                    $operationRow,
                    $ownerType,
                    $ownerType->getType($operationRow['Type'],$operationRow['Classifier'])
                );
                $operations[$operationRow['OperationID']]->errors[$ownerType->getType('Error')->id] = $ownerType->getType('Error');
                if(!empty($operationRow['exception_object_ids'])) foreach(explode(',',$operationRow['exception_object_ids']) as $objectID) {
                    $operations[$operationRow['OperationID']]->errors[$objectID] = $this->types[$objectID];
                }
                $ownerType->operations[] = $operations[$operationRow['OperationID']];
            }

            if(!empty($operations)) foreach($this->dbc->select(sprintf("
                SELECT
                    op.OperationID,
                    op.Name,
                    op.Notes,
                    op.Classifier,
                    op.Type
                FROM t_operationparams op
                WHERE op.OperationID IN (%s)
                ORDER BY Pos
            ",implode(',',array_keys($operations)))) as $operationParamRow) {
                $operationParamRow = array_map('trim',$operationParamRow);
                $ownerOperation = $operations[$operationParamRow['OperationID']];
                $ownerOperation->params[] = new OperationParam(
                    $operationParamRow,
                    $ownerOperation->ownerType->getType($operationParamRow['Type'],$operationParamRow['Classifier']),
                    $ownerOperation
                );
            }
        }

        /** @var Diagram[] $diagrams */
        $diagrams = array();
        if(!empty($this->packages)) foreach($this->dbc->select(sprintf("
            SELECT
                d.Diagram_ID,
                d.Diagram_Type,
                d.Name,
                d.Notes,
                d.Stereotype,
                d.Package_ID
            FROM t_diagram d
            WHERE 1
            AND d.Package_ID IN (%s)
            ",implode(',',array_keys($this->packages)))) as $diagramRow) {
            $diagramRow = array_map('trim',$diagramRow);
            $package = $this->packages[$diagramRow['Package_ID']];
            if(!$package) throw new \Exception("Missing package!?");
            $diagrams[$diagramRow['Diagram_ID']] = ($package->diagrams[] = new Diagram($diagramRow,$package));
        }
        if(!empty($diagrams)) foreach($this->dbc->select(sprintf("
            SELECT
                do.Diagram_ID,
                do.Object_ID
            FROM t_diagramobjects do
            WHERE 1
            AND do.Diagram_ID IN (%s)
            ",implode(',',array_keys($diagrams)))) as $diagramObjectRow) {
            if($this->types[$diagramObjectRow['Object_ID']]) $diagrams[$diagramObjectRow['Diagram_ID']]->types[] = $this->types[$diagramObjectRow['Object_ID']];
        }
    }

    public function printVCPackageList(Package $relativeTo) {
        $sidePackages = array();
        foreach($this->vcPackages as $package) $sidePackages[$package->package->package->name][$package->package->name][] = $package;
        foreach($sidePackages as $tlName => $tlPackages):?>
            <h4><?=self::htmlEscape($tlName)?>:</h4>
            <ul>
                <?php foreach($tlPackages as $ilName => $packages): if(empty($packages))continue; $packages = array_reverse($packages);?>
                    <li>
                        <a href="<?=$relativeTo->getRelativePathTo(reset($packages))?>"><?=self::htmlEscape($ilName)?></a>
                        <?php foreach($packages as $package):?>
                            <small><a href="<?=$relativeTo->getRelativePathTo($package)?>"><?=self::htmlEscape($package->name)?></a></small>
                        <?php endforeach;?>
                    </li>
                <?php endforeach;?>
            </ul>
        <?php endforeach;
    }
}