<?php
namespace EAModelTools\Model;

class OperationParam {
    public $id;
    public $name;
    public $note;

    /** @var Type */
    public $type;

    /** @var Operation */
    public $operation;

    public function __construct($operationParamRow, Type $type, Operation $operation) {
        $this->id = $operationParamRow['OperationID'].'@'.$operationParamRow['Name'];
        $this->name = $operationParamRow['Name'];
        $this->note = Model::processNote($operationParamRow['Notes']);

        $this->type = $type;
        $this->operation = $operation;
    }
}