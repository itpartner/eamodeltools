<?php

namespace EAModelTools\Model;

class Type {
    public $id;
    public $type;# :)
    public $name;
    public $note;
    public $stereotype;

    /**
     * @var Package
     */
    public $package;

    /**
     * @var Attribute[]
     */
    public $attributes = array();

    /**
     * @var string[];
     */
    public $enumOptions;

    /**
     * @var Type
     */
    public $parentType;
    public $parentTypeID;

    /**
     * @var Operation[]
     */
    public $operations = array();

    /**
     * @var \DOMDocument
     */
    public $wsdl;

    public function __construct($dbRow, Package $package = null) {
        $this->id = $dbRow['Object_ID'];
        $this->type = $dbRow['Object_Type'];
        $this->name = $dbRow['Name'];
        $this->note = Model::processNote($dbRow['Note']);
        $this->parentTypeID = $dbRow['parentTypeID'];
        $this->package = $package;
        $this->stereotype = $dbRow['Stereotype'];
    }

    private static $basicTypes = null;
    public static function getBasicTypes() {
        if(!self::$basicTypes) {
            $types = array();
            $pack = Package::getXSDPackage();
            foreach(array(
                        new Type(array('Name' => 'string'), $pack),
                        new Type(array('Name' => 'boolean'), $pack),
                        new Type(array('Name' => 'int'), $pack),
                        new Type(array('Name' => 'long'), $pack),
                        new Type(array('Name' => 'date'), $pack),
                        new Type(array('Name' => 'time'), $pack),
                        new Type(array('Name' => 'dateTime'), $pack),
                        new Type(array('Name' => 'duration'), $pack),
                        new Type(array('Name' => 'decimal'), $pack),
                        new Type(array('Name' => 'double'), $pack),
                        new Type(array('Name' => 'float'), $pack),
                        new Type(array('Name' => 'base64Binary'), $pack),
                    ) as $type) $types[$type->name] = $type;
            $types['bool'] = $types['boolean'];
            $types['char'] = $types['string'];
            $types['Date'] = $types['date'];
            $types['Time'] = $types['time'];
            $types['DateTime'] = $types['dateTime'];
            self::$basicTypes = $types;
        }
        return self::$basicTypes;
    }

    /**
     * @param string $name
     * @param string $classifier
     * @return Type
     */
    public function getType($name, $classifier = null, /** @noinspection PhpUnusedParameterInspection */ $errorInfo = null) {
        $basicTypes = self::getBasicTypes();
        if($basicTypes[$name]) return $basicTypes[$name];
        for($p = $this->package;$p;$p = $p->package) if($p->stereotype == 'VC') {
            $typesByNameAndID = $p->getAllTypes();
            foreach($p->imports as $pi) foreach($pi->getAllTypes() as $types) foreach($types as $type) $typesByNameAndID[$type->name][$type->id] = $type;
            if(empty($typesByNameAndID[$name])) throw new \Exception("Unknown type $name in {$p->name} ({$p->package->name}).");
            if($typesByNameAndID[$name][$classifier]) return $typesByNameAndID[$name][$classifier];
            if(count($typesByNameAndID[$name]) == 1) return reset($typesByNameAndID[$name]);
            throw new \Exception("Ambigous type $name in {$p->name} ({$p->package->name}).");
        }
        throw new \Exception("Unknown type $name. {$this->name} ({$this->package->name}).");
    }

    public function isR() {
        return $this->stereotype == 'R' || $this->package->isR();
    }

    public function getEndpoint() {
        return "{$this->package->nsURI}/{$this->name}";
    }

    public function createWSDL() {
        if($this->package->isNoWSGen()) return;

        $xsdPackage = Package::getXSDPackage();
        $wsdlPackage = Package::getWSDLPackage();
        $soapPackage = Package::getSOAPPackage();
        $ns = array(
            $wsdlPackage->nsURI => 'wsdl',
            $soapPackage->nsURI => 'soap',
            $this->package->nsURI => 'tns',
            $xsdPackage->nsURI => 'xsd'
        );
        /** @var Package[] $imports */
        $imports = array();
        $i=0;
        foreach($this->operations as $operation) {
            if($operation->returnType && !$ns[$operation->returnType->package->nsURI]) {
                $ns[$operation->returnType->package->nsURI] = 'ns' . ++$i;
                $imports[] = $operation->returnType->package;
            }

            foreach($operation->params as $operationParam) {
                if(!$ns[$operationParam->type->package->nsURI]) {
                    $ns[$operationParam->type->package->nsURI] = 'ns' . ++$i;
                    $imports[] = $operationParam->type->package;
                }
            }
            foreach($operation->errors as $operationErr) {
                if(!$ns[$operationErr->package->nsURI]) {
                    $ns[$operationErr->package->nsURI] = 'ns' . ++$i;
                    $imports[] = $operationErr->package;
                }
            }
        }

        $wsdl = new \DOMDocument('1.0','UTF-8');
        $wsdl->formatOutput = true;
        $wsdl->appendChild($definitionsEl = $wsdl->createElementNS($xsdPackage->nsURI,'wsdl:definitions'));
        foreach($ns as $nsURI => $nsAlias) {
            $definitionsEl->setAttributeNS('http://www.w3.org/2000/xmlns/','xmlns:'.$nsAlias,$nsURI);
        }
        $definitionsEl->setAttribute('targetNamespace',$this->package->nsURI);

        $definitionsEl->appendChild($typesEl = $wsdl->createElementNS($wsdlPackage->nsURI,'types'));
        $typesEl->appendChild($schemaEl = $wsdl->createElementNS($xsdPackage->nsURI,'schema'));
        $schemaEl->setAttribute('targetNamespace',$this->package->nsURI);
        $schemaEl->appendChild($includeEl = $wsdl->createElementNS($xsdPackage->nsURI,'include'));
        $includeEl->setAttribute('schemaLocation',$this->package->getRelativePathTo($this->package).'schema.xsd');

        foreach($imports as $importPackage) {
            $schemaEl = $typesEl->appendChild($wsdl->createElementNS($xsdPackage->nsURI,'schema'));
            $schemaEl->appendChild($importEl = $wsdl->createElementNS($xsdPackage->nsURI,'import'));
            $importEl->setAttribute('namespace',$importPackage->nsURI);
            $importEl->setAttribute('schemaLocation',$this->package->getRelativePathTo($importPackage).'schema.xsd');
        }

        foreach($this->operations as $operation) {
            $definitionsEl->appendChild($requestMessageEl = $wsdl->createElementNS($wsdlPackage->nsURI,'message'));
            $requestMessageEl->setAttribute('name',$operation->name);
            foreach($operation->params as $operationParam) {
                $requestMessageEl->appendChild($partEl = $wsdl->createElementNS($wsdlPackage->nsURI,'part'));
                if($note = trim($operationParam->note)) {
                    $partEl->appendChild($documentationEl = $wsdl->createElementNS($wsdlPackage->nsURI,'documentation'));
                    $documentationEl->appendChild($wsdl->createTextNode($note));
                }
                $partEl->setAttribute('name',$operationParam->name);
                if($operationParam->type->package->nsURI == $xsdPackage->nsURI) throw new \Exception("Can not use basic XSD types as message part. (".$this->package->nsURI.'::'.$operation->name.")");
                $partEl->setAttribute($operationParam->type->package->nsURI != $xsdPackage->nsURI ? 'element' : 'type',$ns[$operationParam->type->package->nsURI].':'.$operationParam->type->name);
            }
            $definitionsEl->appendChild($responseMessageEl = $wsdl->createElementNS($wsdlPackage->nsURI,'message'));
            $responseMessageEl->setAttribute('name',$operation->name.'Response');
            $responseMessageEl->appendChild($partEl = $wsdl->createElementNS($wsdlPackage->nsURI,'part'));
            $partEl->setAttribute('name',$operation->returnType->name);
            if($operation->returnType->package->nsURI == $xsdPackage->nsURI) throw new \Exception("Can not use basic XSD types as message part. ($operation->name)");
            $partEl->setAttribute('element',$ns[$operation->returnType->package->nsURI].':'.$operation->returnType->name);

            foreach($operation->errors as $operationError) {
                $definitionsEl->appendChild($faultMessageEl = $wsdl->createElementNS($wsdlPackage->nsURI,'message'));
                $faultMessageEl->setAttribute('name',ucfirst($operation->name).$operationError->name);
                $faultMessageEl->appendChild($partEl = $wsdl->createElementNS($wsdlPackage->nsURI,'part'));
                $partEl->setAttribute('name',$operationError->name);
                if($operationError->package->nsURI == $xsdPackage->nsURI) throw new \Exception("Can not use basic XSD types as message part. ($operation->name)");
                $partEl->setAttribute('element',$ns[$operationError->package->nsURI].':'.$operationError->name);
            }
        }

        $definitionsEl->appendChild($portTypeEl = $wsdl->createElementNS($wsdlPackage->nsURI,'portType'));
        $portTypeEl->setAttribute('name',$this->name);
        if($note = trim($this->note)) {
            $portTypeEl->appendChild($documentationEl = $wsdl->createElementNS($wsdlPackage->nsURI,'documentation'));
            $documentationEl->appendChild($wsdl->createTextNode($note));
        }
        foreach($this->operations as $operation) {
            $portTypeEl->appendChild($operationEl = $wsdl->createElementNS($wsdlPackage->nsURI,'operation'));
            if($note = trim($operation->note)) {
                $operationEl->appendChild($documentationEl = $wsdl->createElementNS($wsdlPackage->nsURI,'documentation'));
                $documentationEl->appendChild($wsdl->createTextNode($note));
            }
            $operationEl->setAttribute('name',$operation->name);
            $operationEl->appendChild($inputEl = $wsdl->createElementNS($wsdlPackage->nsURI,'input'));
            $inputEl->setAttribute('message','tns:'.$operation->name);
            $operationEl->appendChild($outputEl = $wsdl->createElementNS($wsdlPackage->nsURI,'output'));
            $outputEl->setAttribute('message','tns:'.$operation->name.'Response');
            foreach($operation->errors as $operationError) {
                $operationEl->appendChild($faultEl = $wsdl->createElementNS($wsdlPackage->nsURI,'fault'));
                $faultEl->setAttribute('name',ucfirst($operation->name).$operationError->name);
                $faultEl->setAttribute('message','tns:'.ucfirst($operation->name).$operationError->name);
            }
        }

        $definitionsEl->appendChild($bindingEl = $wsdl->createElementNS($wsdlPackage->nsURI,'binding'));
        $bindingEl->setAttribute('name', $this->name.'Binding');
        $bindingEl->setAttribute('type', 'tns:'.$this->name);
        $bindingEl->appendChild($soapBindingEl = $wsdl->createElementNS($soapPackage->nsURI,'binding'));
        $soapBindingEl->setAttribute('transport','http://schemas.xmlsoap.org/soap/http');
        foreach($this->operations as $operation) {
            $bindingEl->appendChild($operationEl = $wsdl->createElementNS($wsdlPackage->nsURI,'operation'));
            $operationEl->setAttribute('name',$operation->name);
            $operationEl->appendChild($soapOperationEl = $wsdl->createElementNS($soapPackage->nsURI,'operation'));
            $soapOperationEl->setAttribute('style','document');
            $soapOperationEl->setAttribute('soapAction',$operation->name);
            $operationEl->appendChild($inputEl = $wsdl->createElementNS($wsdlPackage->nsURI,'input'));
            $inputEl->appendChild($soapBodyEl = $wsdl->createElementNS($soapPackage->nsURI,'body'));
            $soapBodyEl->setAttribute('use','literal');
            $operationEl->appendChild($outputEl = $wsdl->createElementNS($wsdlPackage->nsURI,'output'));
            $outputEl->appendChild($soapBodyEl = $wsdl->createElementNS($soapPackage->nsURI,'body'));
            $soapBodyEl->setAttribute('use','literal');
            foreach($operation->errors as $operationError) {
                $operationEl->appendChild($faultEl = $wsdl->createElementNS($wsdlPackage->nsURI,'fault'));
                $faultEl->setAttribute("name",ucfirst($operation->name).$operationError->name);
                $faultEl->appendChild($soapFaultEl = $wsdl->createElementNS($soapPackage->nsURI,'fault'));
                $soapFaultEl->setAttribute('name',ucfirst($operation->name).$operationError->name);
                $soapFaultEl->setAttribute('use','literal');
            }
        }

        $definitionsEl->appendChild($serviceEL = $wsdl->createElementNS($wsdlPackage->nsURI,'service'));
        $serviceEL->setAttribute('name', $this->name);
        $serviceEL->appendChild($servicePortEl = $wsdl->createElementNS($wsdlPackage->nsURI,'port'));
        $servicePortEl->setAttribute('name',$this->name);
        $servicePortEl->setAttribute('binding','tns:'.$this->name.'Binding');
        $servicePortEl->appendChild($serviceSoapEL = $wsdl->createElementNS($soapPackage->nsURI,'address'));
        $serviceSoapEL->setAttribute('location', $this->getEndpoint().'.soap');


        $this->wsdl = $wsdl;
    }

    public function printAttributesHTML(Package $relativeTo) {
        ?>
        <div class="panel panel-default" id="<?=Model::htmlEscape($this->name)?>">
            <div class="panel-heading">
                <h4 style="" class="type-title"><?=Model::htmlEscape($this->name)?> <small>(<?=Model::htmlEscape($this->type)?>)</small></h4>
                <div class="note"><?=($this->note)?></div>
            </div>
            <?php if($this->type == 'Enumeration'):?>
                <table class="enum-table table table-striped">
                    <tr>
                        <th>Value</th>
                        <th>Description</th>
                    </tr>
                    <?php foreach($this->attributes as $attribute):?>
                        <tr>
                            <td class="attribute-name"><?=Model::htmlEscape($attribute->name)?></td>
                            <td class="attribute-description"><div class="note"><?=($attribute->note)?></div></td>
                        </tr>
                    <?php endforeach;?>
                </table>
            <?php else:?>
                <table class="type-table table table-striped">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>Type</th>
                        <th>Multiplicity</th>
                        <th>Description</th>
                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($this->attributes as $attribute):?>
                        <tr>
                            <td class="attribute-name"><?=Model::htmlEscape($attribute->name)?></td>
                            <td class="attribute-type"><a href="<?=$relativeTo->typeURL($attribute->type)?>"><?=Model::htmlEscape($attribute->type->name)?></a></td>
                            <td class="attribute-multiplicity"><?=Model::htmlEscape('['.$attribute->lowerBound.'..'.$attribute->upperBound.']')?></td>
                            <td class="attribute-description"><div class="note"><?=($attribute->note)?></div></td>
                        </tr>
                    <?php endforeach;?>
                    </tbody>
                </table>
            <?php endif;?>
        </div>
        <?php
    }
    public function printOperationsHTML(Package $relativeTo) {
        ?>
        <h3 class="service-title" id="<?=Model::htmlEscape($this->name)?>"><?=Model::htmlEscape($this->name)?> <small>(<?=Model::htmlEscape($this->type)?>)</small></h3>
        <?php foreach($this->operations as $operation) {
            $operation->printHTML($relativeTo);
        }
    }

    public function getPHPFQN() {
        return $this->package->getPHPNS()."\\".$this->name;
    }

    private static $phpStringTypes = array(
        'base64Binary' => 1,
        'date' => 1,
        'Date' => 1,
        'dateTime' => 1,
        'DateTime' => 1,
        'Time' => 1,
        'time' => 1,
        'duration' => 1,
        'Duration' => 1,
        'decimal' => 1,
        'Decimal' => 1,
    );
    public function getPHPName() {
        if($this->type == 'Enumeration' || self::$phpStringTypes[$this->name]) {
            return 'string';
        } else {
            return $this->name;
        }
    }

    public function isPHPExportable() {
        $basicTypes = $this->getBasicTypes();
        if(!preg_match("/^[\\\\a-z\\d_]+$/i",$this->getPHPFQN())) return false;
        if($basicTypes[$this->name]) return false;
        return $this->package->getVCPackage() && $this->package->getPHPNS() && ($this->type == "Interface" || $this->type == "Class");
    }

    /**
     * @return Attribute[]
     */
    public function getParentAttributes() {
        $attributes = array();
        for($parent = $this->parentType; $parent; $parent = $parent->parentType) foreach($parent->attributes as $attribute) $attributes[] = $attribute;
        return $attributes;
    }

    /**
     * @param Type[] $types
     */
    public function getDependancyTypes($types = null) {
        if(!$types) $types = array();
        /** @var Type[] $lookupTypes */
        $lookupTypes = array($this->id => $this);
        foreach($this->attributes as $attribute) $lookupTypes[$attribute->type->id] = $attribute->type;
        foreach($this->operations as $operation) {
            if($operation->returnType) $lookupTypes[$operation->returnType->id] = $operation->returnType;
            foreach($operation->params as $param) $lookupTypes[$param->type->id] = $param->type;
            foreach($operation->errors as $error) $lookupTypes[$error->id] = $error;
        }
        unset($lookupTypes['']);
        foreach($lookupTypes as $type) {
            if(!$types[$type->id]) {
                $types[$type->id] = $type;
                foreach($type->getDependancyTypes($types) as $innerType) $types[$innerType->id] = $innerType;
            }
        }

        return $types;
    }

    public function createPHPClass() {
        if(!$this->isPHPExportable()) return null;
        $src = array();
        $src[] = "<?php";
        $src[] = "namespace " . $this->package->getPHPNS() . ";";

        $src[] = '';

        /** @var Attribute[] $attributes */
        $attributes = array_merge($this->attributes, $this->getParentAttributes());

        $imports = array();
        if(!empty($this->operations) || !empty($this->attributes)) $imports['econt\\SOA\\SoapUtil'] = 1;

        foreach ($this->operations as $operation) {

            if($operation->returnType->isPHPExportable() && $operation->returnType->package->getPHPNS() != $this->package->getPHPNS()) {
                $imports[$operation->returnType->getPHPFQN()] = 1;
            }
            foreach ($operation->params as $operationParam) {
                if($operationParam->type->isPHPExportable() && $operationParam->type->package->getPHPNS() != $this->package->getPHPNS()) {
                    $imports[$operationParam->type->getPHPFQN()] = 1;
                }
            }
        }
        foreach ($attributes as $attribute) {
            if($attribute->type->isPHPExportable() && $attribute->type->package->getPHPNS() != $this->package->getPHPNS()) {
                $imports[$attribute->type->getPHPFQN()] = 1;
            }
        }

        foreach (array_keys($imports) as $imp) $src[] = "use " . $imp . ";";
        if(array_keys($imports)) $src[] = '';

        if($this->attributes || $this->note || $this->parentType->attributes) $src[] = "/**";
        if($this->note) {
            $src[] = " * {$this->note}";
            if($this->attributes) $src[] = " *";
        }
        foreach ($attributes as $attribute) {
            $arr = $attribute->isArray() ? '[]' : '';
            $src[] = " * @property " . $attribute->type->getPHPName() . $arr . ' $' . $attribute->name . ($attribute->note ? " {$attribute->note}" : '');
        }
        if($this->attributes || $this->note || $this->parentType->attributes ) $src[] = " */";

        if($this->type == "Interface") {

            $src[] = "abstract class " . $this->name . " {";

            $classMap = array();
            foreach($this->getDependancyTypes() as $type) {
                $classMap[$type->name] = $type->getPHPFQN();
            }

            $src[] = '';
            $src[] = '    public static $CLASS_MAP = array(';
            foreach($classMap as $name => $FQN) {
                $src[] = '        \''.$name.'\' => \''.$FQN.'\',';
            }
            $src[] = '    );';

        }
        else $src[] = "class " . $this->name . " implements \\JsonSerializable {";
        $src[] = '';
        $src[] = '    const NS = \'' . $this->package->nsURI . '\';';
        if(!$attributes && $this->operations) {
            $privateServices = array();
            foreach ($this->operations as $operation) if($operation->scope === 'Private') $privateServices[] = $operation->name;
            if($privateServices) {
                $src[] = '';
                $src[] = sprintf("    const PRIVATE_METHODS = '%s';", implode(', ', $privateServices));
            }
        }
        if(!empty($attributes)) {
            $src[] = '';

            $src[] = '    private static $propertyTypes = array(';
            foreach ($attributes as $attribute) {
                if(self::$phpStringTypes[$attribute->type->name]) $typeName = $attribute->type->name;
                else $typeName = ltrim($attribute->type->getPHPFQN(), '\\');
                if($attribute->type->type == 'Enumeration') $typeName = 'string';
                $src[] = "        '$attribute->name' => '" . $typeName . ($attribute->isArray() ? '[]' : '') . "',";
            }
            $src[] = '    );';

            $src[] = '';

            foreach ($attributes as $attribute) $src[] = "    private /** @noinspection PhpUnusedPrivateFieldInspection */" . ' $' . $attribute->name . " = " . ($attribute->isArray() ? 'array()' : 'null') . ';';

            $src[] = '';
            $src[] = '    public function __isset($name) {';
            $src[] = '        return isset($this->$name);';
            $src[] = '    }';
            $src[] = '';

            $src[] = '    public function &__get($name) {';
            $src[] = '        if(property_exists(__CLASS__,$name)) {';
            $src[] = '            if(substr(self::$propertyTypes[$name],-2) == \'[]\' && !is_array($this->$name)) {';
            $src[] = '                if($this->$name != null) $this->$name = array($this->$name);';
            $src[] = '                else $this->$name = array();';
            $src[] = '            }';
            $src[] = '            if(self::$propertyTypes[$name] == \'dateTime\' || self::$propertyTypes[$name] == \'date\' || self::$propertyTypes[$name] == \'time\') return SoapUtil::soapToPhpDateTime($this->$name);';
            $src[] = '            return $this->$name;';
            $src[] = '        } else {';
            $src[] = '            return null;';
            $src[] = '        }';
            $src[] = '    }';
            $src[] = '    public function __set($name, $value) {';
            $src[] = '        if(!self::$propertyTypes[$name]) return null;';
            $src[] = '        if(substr(self::$propertyTypes[$name],-2) == \'[]\') {';
            $src[] = '            $type = substr(self::$propertyTypes[$name],0,-2);';
            $src[] = '            $isArray = true;';
            $src[] = '        } else {';
            $src[] = '            $type = self::$propertyTypes[$name];';
            $src[] = '            $isArray = false;';
            $src[] = '        }';
            $src[] = '        if($value === null) {';
            $src[] = '            $this->$name = $isArray ? array() : null;';
            $src[] = '            return;';
            $src[] = '        }';
            $src[] = '        switch($type) {';
            $src[] = '            case \'base64binary\':';
            $src[] = '            case \'base64Binary\':';
            $src[] = '            case \'duration\':';
            $src[] = '            case \'string\':';
            $src[] = '            case \'time\':';
            $src[] = '                if ($isArray) {';
            $src[] = '                    $a = array();';
            $src[] = '                    if ($value && is_array($value)) foreach ($value as $v) $a[] = (string)$v;';
            $src[] = '                    $this->$name = $a;';
            $src[] = '                } else {';
            $src[] = '                    $this->$name = (string)$value;';
            $src[] = '                }';
            $src[] = '                break;';
            $src[] = '            case \'int\':';
            $src[] = '            case \'integer\':';
            $src[] = '            case \'long\':';
            $src[] = '                if ($isArray) {';
            $src[] = '                    $a = array();';
            $src[] = '                    if ($value && is_array($value)) foreach ($value as $v) $a[] = (int)$v;';
            $src[] = '                    $this->$name = $a;';
            $src[] = '                } else {';
            $src[] = '                    $this->$name = (int)$value;';
            $src[] = '                }';
            $src[] = '                break;';
            $src[] = '            case \'float\':';
            $src[] = '            case \'double\':';
            $src[] = '            case \'decimal\':';
            $src[] = '                $f = $type == \'decimal\' ? function($v){return str_replace(array(\',\', \' \'), array(\'.\', \'\'), $v);} : function($v){return (double)str_replace(array(\',\', \' \'), array(\'.\', \'\'), $v);};';
            $src[] = '                if($isArray) {';
            $src[] = '                    $this->$name = is_array($value) ? array_map($f,$value) : array();';
            $src[] = '                } else {';
            $src[] = '                    $this->$name = $f($value);';
            $src[] = '                }';
            $src[] = '                break;';
            $src[] = '            case \'bool\':';
            $src[] = '            case \'boolean\':';
            $src[] = '                if ($isArray) {';
            $src[] = '                    $a = array();';
            $src[] = '                    if ($value && is_array($value)) foreach ($value as $v) $a[] = (boolean)$v;';
            $src[] = '                    $this->$name = $a;';
            $src[] = '                } else {';
            $src[] = '                    $this->$name = (boolean)$value;';
            $src[] = '                }';
            $src[] = '                break;';
            $src[] = '            case \'date\':';
            $src[] = '                $this->$name = $isArray ? array_map([\econt\SOA\SoapUtil::class, \'phpToSoapDate\'], $value, [$type])  : SoapUtil::phpToSoapDate($value, $type);';
            $src[] = '                break;';
            $src[] = '            case \'dateTime\':';
            $src[] = '                $this->$name = $isArray ? array_map([\econt\SOA\SoapUtil::class, \'phpToSoapDateTime\'], $value, [$type])  : SoapUtil::phpToSoapDateTime($value, $type);';
            $src[] = '                break;';
            $src[] = '            default:';
            $src[] = '                $fv = array();';
            $src[] = '                foreach ((!$isArray ? array($value) : $value) as $val) {';
            $src[] = '                    if (is_object($val)) {';
            $src[] = '                        if(get_class($val) != $type) throw new \econt\classes\ExException(sprintf("Can\'t assign %s to %s",get_class($val),$type));';
            $src[] = '                        $fv[] = $val;';
            $src[] = '                    } else {';
            $src[] = '                        $fv[] = call_user_func_array($type . \'::fromArray\', array($val));';
            $src[] = '                    }';
            $src[] = '                }';
            $src[] = '                $this->$name = $isArray ? $fv : reset($fv);';
            $src[] = '        }';
            $src[] = '    }';
            $src[] = '';
            $src[] = '    public function __clone() {';
            $src[] = '    	foreach (self::$propertyTypes as $name => $type) {';
            $src[] = '    		$type = trim($type,\'[]\');';
            $src[] = '    		if (!$this->$name || $type == \'base64binary\' || $type == \'duration\' || $type == \'string\' || $type == \'int\' || $type == \'integer\' || $type == \'long\' || $type == \'float\' || $type == \'double\' || $type == \'decimal\' || $type == \'bool\' || $type == \'boolean\' || $type == \'date\' || $type == \'dateTime\' || $type == \'time\') continue;';
            $src[] = '    		';
            $src[] = '    		if (!is_array($this->$name)) {';
            $src[] = '    			$this->$name = clone $this->$name;';
            $src[] = '    		} else {';
            $src[] = '    			$new = array();';
            $src[] = '    			foreach ($this->$name as $k => $v) {';
            $src[] = '    				$new[$k] = clone $v;';
            $src[] = '    			}';
            $src[] = '    			$this->$name = $new;';
            $src[] = '    		}';
            $src[] = '    	}';
            $src[] = '    }';
            $src[] = '';
            $src[] = "    /**";
            foreach ($attributes as $attribute) {
                /** @var Attribute $attribute */
                $arr = $attribute->isArray() ? '[]' : '';
                $src[] = "     * @param " . $attribute->type->getPHPName() . $arr . ' $' . $attribute->name . ($attribute->note ? " {$attribute->note}" : '');
            }
            $src[] = "     */";
            $constructorParams = implode(', ',array_map(function (Attribute $attribute) {
                return '$' . $attribute->name . ' = null';
            }, $attributes));
            $src[] = "    public function __construct($constructorParams) {";
            foreach ($attributes as $attribute) $src[] = '        $this->__set("' . $attribute->name . '", $' . $attribute->name . ');';
            $src[] = "    }";

            $src[] = '';

            $src[] = '    public static function fromArray($array) {';
            $src[] = '	      if(!is_array($array)) $array = array();';
            $src[] = "        return new {$this->name}(";
            foreach ($attributes as $attribute) $src[] = "            \$array['{$attribute->name}'],";
            $src[] = rtrim(array_pop($src), ',');
            $src[] = '        );';
            $src[] = '    }';

            $src[] = '';

            $src[] = '    public static function fromFlatArray(array $array, $prefix = \'\') {';
            $src[] = '        $b = false;';
            $src[] = '        $keys = array();';
            $src[] = '        $prefixLen = strlen($prefix);';
            $src[] = '        foreach($array as $k => $v) {';
            $src[] = '            if(!($prefix && strpos($k, $prefix) !== 0) && $v !== null) {';
            $src[] = '                $b = true;';
            $src[] = '                $keys[substr($k, $prefixLen, strpos($k, \'_\', $prefixLen) - $prefixLen)] = true;';
            $src[] = '            }';
            $src[] = '        }';
            $src[] = '        if(!$b) return null;';
            $src[] = '        $res = new self();';
            $src[] = '        foreach (self::$propertyTypes as $name => $type) {';
            $src[] = '            if (substr(self::$propertyTypes[$name], -2) !== \'[]\') $isArray = false;';
            $src[] = '            else {';
            $src[] = '                $isArray = true;';
            $src[] = '                $type = substr($type, 0, -2);';
            $src[] = '            }';
            $src[] = '            if (isset($keys[$name])) {';
            $src[] = '                if ($isArray) throw new \econt\classes\ExException("Can\'t create array properties from flat array.");';
            $src[] = '                $res->__set($name, call_user_func_array("{$type}::fromFlatArray", array($array, "{$prefix}{$name}_")));';
            $src[] = '            } else {';
            $src[] = '                if ($isArray) $res->__set($name, empty($array["{$prefix}{$name}"]) ? array() : explode(\',\', $array["{$prefix}{$name}"]));';
            $src[] = '                else $res->__set($name, $array["{$prefix}{$name}"]);';
            $src[] = '            }';
            $src[] = '        }';
            $src[] = '        return $res;';
            $src[] = '    }';

            $src[] = '';

            $src[] = '    public function jsonSerialize() {';
//            if($this->parentType) $src[] = '        $res = parent::jsonSerialize();';
//            else $src[] = '        $res = array();';
            $src[] = '        $res = array();';
            $src[] = '        foreach($this as $k => $v) {';
            $src[] = '                if ($v === null) $res[$k] = null;';
            $src[] = '                else if (in_array(self::$propertyTypes[$k], array(\'date\', \'dateTime\', \'time\'))) $res[$k] = strtotime($this->__get($k)) * 1000;';
            $src[] = '                else $res[$k] = $this->__get($k);';
            $src[] = '            }';
            $src[] = '            return $res;';
            $src[] = '    }';
        } else if(!empty($this->operations)) {
            $src[] = '';
            $src[] = "    /**";
            $src[] = "     * @return ".$this->name;
            $src[] = "     */";
            $src[] = '    public static function instance() {';
            $src[] = '        return SoapUtil::clientInstance(__CLASS__);';
            $src[] = '    }';
            $src[] = '';
            foreach ($this->operations as $operation) {
                $src[] = '';
                $src[] = "    /**";
                if($operation->note) {
                    $src[] = "     * {$operation->note}";
                    $src[] = '     *';
                }
                foreach ($operation->params as $operationParam) {
                    $src[] = "     * @param " . $operationParam->type->getPHPName() . ' $' . $operationParam->name . ($operationParam->type->note ? " {$operationParam->type->note}" : '');
                }
                if($operation->returnType) {
                    $src[] = "     * @return " . $operation->returnType->getPHPName();
                }
                $src[] = "     * @throws \\Exception";

                $src[] = "     */";
                $src[] = '    public abstract function ' . $operation->name . '(' . implode(array_map(function (OperationParam $param) {
                        return $param->type->getPHPName() . ' $' . $param->name . ' = null';
                    }, $operation->params)) . ');';
            }
        } else {
            $src[] = '';
            $src[] = '    function jsonSerialize() {}';
        }
        $src[] = '';
        $src[] = "}";
        return implode("\n", $src);
    }
}
