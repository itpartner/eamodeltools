<?php
namespace EAModelTools\Model;

class Operation {
    public $id;
    public $name;
    public $note;
    public $stereotype;

    /** @var Type */
    public $returnType;

    /** @var Type */
    public $ownerType;

    /**
     * @var OperationParam[]
     */
    public $params = array();

    /** @var Type[] */
    public $errors = array();

    public function __construct($operationRow, Type $ownerType, Type $returnType) {
        $this->id = $operationRow['OperationID'];
        $this->name = $operationRow['Name'];
        $this->note = Model::processNote($operationRow['Notes']);
        $this->stereotype = $operationRow['Stereotype'];

        $this->ownerType = $ownerType;
        $this->returnType = $returnType;
    }

    public function getEndpoint($type) {

        if($type == 'soap') return $this->ownerType->getEndpoint().'.soap?wsdl';
        elseif($type == 'rest.json') return $this->ownerType->getEndpoint().".".$this->name.".json";
        return '';
    }

    public function printHTML(Package $relativeTo) {
        if(count($this->params) == 1 && $this->params[0]->type->isR()) {
            $paramList = $this->params[0]->type->attributes;
            $note = $this->params[0]->type->note ? $this->params[0]->type->note : $this->note;
        } else {
            $paramList = $this->params;
            $note = $this->note;
        }
        ?>
        <div class="panel panel-default">
            <div class="panel-heading" id="<?="{$this->ownerType->name}-{$this->name}"?>">
                <div class="pull-right" style="">
                    <em>endpoints:</em>
                    <a target="_blank" href="<?=Model::htmlEscape($this->getEndpoint('soap'))?>">soap</a>
                    <a target="_blank" href="<?=Model::htmlEscape($this->getEndpoint('rest.json'))?>">json</a>
                </div>
                <h4 class="operation-title" style=""><?=($this->name)?> <small>(Method)</small></h4>
                <div class="note"><?=($note)?></div>
            </div>
            <table class="type-table table table-striped">
                <thead>
                <tr><th colspan="4"><h5 class="method-sub-title">Parameters</h5></th></tr>
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Multiplicity</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <?php
                foreach($paramList as $param):?>
                    <tr>
                        <td class="parameter-name"><?=Model::htmlEscape($param->name)?></td>
                        <td class="parameter-type"><a href="<?=$relativeTo->typeURL($param->type)?>"><?=Model::htmlEscape($param->type->name)?></a></td>
                        <td class="parameter-multiplicity"><?=$param instanceof Attribute ? Model::htmlEscape('['.$param->lowerBound.'..'.$param->upperBound.']') : ''?></td>
                        <td class="parameter-description"><div class="note"><?=($param->note)?></div></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
            <table class="type-table table table-striped">
                <thead>
                <tr><th colspan="4"><h5 class="method-sub-title">Result</h5></th></tr>
                <tr>
                    <th>Name</th>
                    <th>Type</th>
                    <th>Multiplicity</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <?php if($this->returnType->isR()):foreach($this->returnType->attributes as $attributes):?>
                    <tr>
                        <td class="return-name"><?=Model::htmlEscape($attributes->name)?></td>
                        <td class="return-type"><a href="<?=$relativeTo->typeURL($attributes->type)?>"><?=Model::htmlEscape($attributes->type->name)?></a></td>
                        <td class="return-multiplicity"><?=Model::htmlEscape('['.$attributes->lowerBound.'..'.$attributes->upperBound.']')?></td>
                        <td class="return-description"><div class="note"><?=($attributes->note)?></div></td>
                    </tr>
                <?php endforeach;else:?>
                    <tr>
                        <td class="return-name"></td>
                        <td class="return-type"><a href="<?=$relativeTo->typeURL($this->returnType)?>"><?=Model::htmlEscape($this->returnType->name)?></a></td>
                        <td class="return-multiplicity"></td>
                        <td class="return-description"></td>
                    </tr>
                <?php endif;?>
                </tbody>
            </table>
            <table class="type-table table table-striped">
                <thead>
                <tr><th colspan="2"><h5 class="method-sub-title">Faults</h5></th></tr>
                <tr>
                    <th>Type</th>
                    <th>Description</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($this->errors as $error):?>
                    <tr>
                        <td class="error-type"><a href="<?=$relativeTo->typeURL($error)?>"><?=Model::htmlEscape($error->name)?></a></td>
                        <td class="error-description"><div class="note"><?=($error->note)?></div></td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>
        </div>
        <?php
    }
}