<?php
namespace EAModelTools\Model;

class Attribute {
    public $id;
    public $name;
    public $note;
    public $stereotype;

    /** @var Type */
    public $type;

    /** @var Type */
    public $ownerType;

    public $lowerBound;
    public $upperBound;

    public function __construct($dbRow, Type $ownerType,Type $attributeType = null) {
        $this->id = $dbRow['ID'];
        $this->name = $dbRow['Name'];
        $this->note = Model::processNote($dbRow['Notes']);
        $this->stereotype = $dbRow['Stereotype'];
        $this->lowerBound = $dbRow['LowerBound'];
        $this->upperBound = $dbRow['UpperBound'];

        $this->type = $attributeType;
        $this->ownerType = $ownerType;
    }

    public function isArray() {
        return $this->upperBound == 'n' || $this->upperBound == 'unbounded' || $this->upperBound > 1;
    }
}