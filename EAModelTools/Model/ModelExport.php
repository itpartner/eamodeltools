<?php
namespace EAModelTools\Model;

class ModelExport {
    public $model;
    public function __construct(Model $model) {
        $this->model = $model;
    }

    public function exportModel($dirPath, Package $package = null) {
        if(empty($dirPath)) throw new \Exception;
        if(!$package) $package = $this->model->packages[1];
        //if(file_exists($dirPath)) throw new \Exception("$dirPath already exists.");
        self::rmDir($dirPath);
        mkdir($dirPath);

        if($package->xsd) if(!file_put_contents($dirPath.'/schema.xsd',$package->xsd->saveXML())) throw new \Exception("Unable to write schema.xsd in $dirPath.");
        if($package->html) if(!file_put_contents($dirPath.'/index.html',$package->html->saveHTML())) throw new \Exception("Unable to write index.html in $dirPath.");
        foreach($package->types as $type) {
            if($type->wsdl) if(!file_put_contents($dirPath.'/'.$type->name.'.wsdl',$type->wsdl->saveXML())) throw new \Exception("Unable to write {$type->name}.wsdl in $dirPath.");
        }
        foreach($package->diagrams as $diagram) {
            if($diagram->html) if(!file_put_contents($dirPath.'/'.$diagram->name.'.html',$diagram->html->saveHTML())) throw new \Exception("Unable to write {$diagram->name}.html in $dirPath.");
        }

        foreach($package->packages as $subPackage) $this->exportModel($dirPath.'/'.$subPackage->name,$subPackage);
    }

    public function exportPHP($dirPath, Package $package = null) {
        if(empty($dirPath)) throw new \Exception;
        $c = 0;
        self::rmDir($dirPath);
        mkdir($dirPath);
        if(!$package) {
            $package = $this->model->packages[1];
            $c++;
            file_put_contents($dirPath."/inc.php",implode("\n",array(
                '<?php',
                'namespace econt\\SOA;',
                'spl_autoload_register(function($name) {',
                '    if(!preg_match("/^econt\\\\\\\\SOA\\\\\\\\([a-zA-Z\\\\d\\\\\\\\]+)$/",$name,$matches)) return;',
                '    include dirname(__FILE__)."/".str_replace("\\\\","/",$matches[1]).".php";',
                '});',
                'if(!class_exists("econt\\\\SOA\\\\SoapUtil")) {',
                '    class SoapUtilTemplate {',
                '        public static function clientInstance($class) {',
                '           return null;',
                '        }',
                '',
                '        public static function phpToSoapDateTime($val) {',
                '           return $val;',
                '        }',
                '',
                '        public static function soapToPhpDateTime($val) {',
                '           return $val;',
                '        }',
                '',
                '        public static function soapFaultToException(\SoapFault $fault) {',
                '            return $fault;',
                '        }',
                '    }',
                '}',
            )));
        }

        foreach($package->types as $type) {
            $src = $type->createPHPClass();

            if($src) {
                if(!file_put_contents($dirPath.'/'.$type->name.'.php',$src)) throw new \Exception("Unable to write {$type->name}.wsdl in $dirPath.");
                $c++;
            }
        }

        foreach($package->packages as $subPackage) $c += $this->exportPHP($dirPath.'/'.$subPackage->name,$subPackage);
        if(!$c) rmdir($dirPath);
        return $c;
    }

    private static function rmDir($dirPath) {
        if(!file_exists($dirPath)) return;
        foreach(new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($dirPath, \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::CHILD_FIRST) as $path) {
            $path->isDir() && !$path->isLink() ? rmdir($path->getPathname()) : unlink($path->getPathname());
        }
        rmdir($dirPath);
    }
}