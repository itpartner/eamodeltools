<?php

namespace EAModelTools\Model;

class Package {
    public $id;
    public $name;
    public $note;
    public $stereotype;

    public $nsURI;

    public $typeID;

    /**
     * @var Package
     */
    public $package;

    /**
     * @var Package[]
     */
    public $packages = array();

    /**
     * @var Type[]
     */
    public $types = array();

    /**
     * @var \DOMDocument
     */
    public $xsd;

    /**
     * @var \DOMDocument
     */
    public $html;

    /**
     * @var Package[]
     */
    public $imports = array();

    /** @var  Model */
    public $model;

    /** @var Diagram[] */
    public $diagrams = array();

    public function __construct($dbRow, Package $package = null) {
        $this->id = $dbRow['Package_ID'];
        $this->name = $dbRow['Name'];
        $this->note = Model::processNote($dbRow['Note']);
        $this->stereotype = $dbRow['Stereotype'];
        $this->typeID = $dbRow['Object_ID'];
        $this->package = $package;
        $this->nsURI = $dbRow['namespaceURI'] ? $dbRow['namespaceURI'] : ($this->package && $this->package->nsURI ? $this->package->nsURI."/".$this->name : null);
    }

    private static $xsdPackage = null;
    public static function getXSDPackage() {
        if(!self::$xsdPackage) self::$xsdPackage = new Package(array('Name' => 'XSD', 'namespaceURI' => 'http://www.w3.org/2001/XMLSchema'));
        return self::$xsdPackage;
    }
    private static $wsdlPackage = null;
    public static function getWSDLPackage() {
        if(!self::$wsdlPackage) self::$wsdlPackage = new Package(array('Name' => 'XSD', 'namespaceURI' => 'http://schemas.xmlsoap.org/wsdl/'));
        return self::$wsdlPackage;
    }
    private static $soapPackage = null;
    public static function getSOAPPackage() {
        if(!self::$soapPackage) self::$soapPackage = new Package(array('Name' => 'XSD', 'namespaceURI' => 'http://schemas.xmlsoap.org/wsdl/soap/'));
        return self::$soapPackage;
    }

    public function isR() {
        for($rPackage = $this;$rPackage;$rPackage = $rPackage->package) if($rPackage->stereotype == 'R') return true;
        return false;
    }

    public function getPHPNS() {
        if(strpos($this->nsURI,'http://www.econt.com/Model') === 0) return str_replace("/","\\",str_replace('http://www.econt.com/Model/','econt/SOA/',$this->nsURI));
        if(strpos($this->nsURI,'http://ee.econt.com/services') === 0) return str_replace("/","\\",str_replace('http://ee.econt.com/services','econt/SOA/EEcont',$this->nsURI));
        return null;
    }

    /**
     * @return Package
     */
    public function getVCPackage() {
        for($VCPackage = $this;$VCPackage;$VCPackage = $VCPackage->package) if($VCPackage->stereotype == 'VC') return $VCPackage;
        return null;
    }

    /** @var Type[][] */
    private $allTypesByNameAndID = null;
    public function getAllTypes() {
        if($this->allTypesByNameAndID) return $this->allTypesByNameAndID;
        $this->allTypesByNameAndID = array();
        foreach($this->types as $type) $this->allTypesByNameAndID[$type->name][$type->id] = $type;
        foreach($this->packages as $package) foreach($package->getAllTypes() as $types) foreach($types as $type) $this->allTypesByNameAndID[$type->name][$type->id] = $type;
        return $this->allTypesByNameAndID;
    }

    public function getRelativePathTo(Package $package) {
        $impP = $package;
        $rp = array();
        $pc=0;
        while(1) {
            $myP = $this;
            $pc=0;
            if($myP->id == $impP->id) break;
            while($myP = $myP->package) {
                $pc++;
                if($myP->id == $impP->id) break 2;
            }
            $rp[] = $impP->name.'/';
            if(!$impP = $impP->package) throw new \Exception("Can't find common base package between {$package->name}($package->id) and {$this->name}($this->id).");

        }
        return str_repeat('../',$pc).implode('',array_reverse($rp));
    }

    public function createSchema() {
        if($this->isNoWSGen()) return;
        $xsdPackage = Package::getXSDPackage();
        $ns = array(
            $xsdPackage->nsURI => 'xsd',
            $this->nsURI => 'tns',
        );
        $i=0;
        $imports = array();
        foreach($this->types as $type) {
            if($type->parentType && !$ns[$type->parentType->package->nsURI]) {
                $ns[$type->parentType->package->nsURI] = 'ns' . ++$i;
                $imports[] = $type->parentType->package;
            }
            if($type->type != 'Class') continue;
            foreach ($type->attributes as $attribute) {
                if(!$ns[$attribute->type->package->nsURI]) {
                    $ns[$attribute->type->package->nsURI] = 'ns' . ++$i;
                    $imports[] = $attribute->type->package;
                }
            }
        }

        $xsd = new \DOMDocument('1.0','UTF-8');
        $xsd->formatOutput = true;
        $xsd->appendChild($schema = $xsd->createElementNS($xsdPackage->nsURI,'xsd:schema'));
        foreach($ns as $nsURI => $nsAlias) {
            if($nsURI == $xsdPackage->nsURI) continue;
            $schema->setAttributeNS('http://www.w3.org/2000/xmlns/','xmlns:'.$nsAlias,$nsURI);
        }
        $schema->setAttribute('targetNamespace',$this->nsURI);
        $schema->setAttribute('elementFormDefault','qualified');


        foreach($imports as $importPackage) {
            /** @var Package $importPackage */
            $schema->appendChild($importEl = $xsd->createElementNS($xsdPackage->nsURI,'import'));
            $importEl->setAttribute('namespace', $importPackage->nsURI);
            $importEl->setAttribute('schemaLocation', $this->getRelativePathTo($importPackage).'schema.xsd');
        }

        foreach($this->types as $type) {
            $typeEl = null;
            if($type->package->nsURI == $xsdPackage->nsURI) {//base type
                throw new \Exception("?!?!");
            } else if($type->type == 'Enumeration') {
                $schema->appendChild($typeEl = $xsd->createElementNS($xsdPackage->nsURI,'simpleType'));
                $typeEl->setAttribute('name',$type->name);
                $typeEl->appendChild($restrictionEl = $xsd->createElementNS($xsdPackage->nsURI,'restriction'));
                $restrictionEl->setAttribute('base',$ns[$xsdPackage->nsURI].':string');
                foreach($type->attributes as $attribute) {
                    $restrictionEl->appendChild($enumEl = $xsd->createElementNS($xsdPackage->nsURI,'enumeration'));
                    $enumEl->setAttribute('value',$attribute->name);
                    if($note = trim($attribute->note)) {
                        $enumEl->appendChild($annotationEl = $xsd->createElementNS($xsdPackage->nsURI,'annotation'));
                        $annotationEl->appendChild($documentationEl = $xsd->createElementNS($xsdPackage->nsURI,'documentation'));
                        $documentationEl->appendChild($xsd->createTextNode($note));
                    }
                }
            } else if(($type->type == 'Class')) {
                $schema->appendChild($typeEl = $xsd->createElementNS($xsdPackage->nsURI,'complexType'));
                $typeEl->setAttribute('name',$type->name);


                if($type->parentType || !empty($type->attributes)) {$seqEl = $xsd->createElementNS($xsdPackage->nsURI,'sequence');
                    if($type->parentType) {
                        $typeEl->appendChild($complexContentEl = $xsd->createElementNS($xsdPackage->nsURI,'complexContent'));
                        $complexContentEl->appendChild($extensionEl = $xsd->createElementNS($xsdPackage->nsURI,'extension'));
                        $extensionEl->setAttribute('base',$ns[$type->parentType->package->nsURI].":".$type->parentType->name);
                        $extensionEl->appendChild($seqEl);
                    } else {
                        $typeEl->appendChild($seqEl);
                    }

                    foreach($type->attributes as $attribute) {
                        $seqEl->appendChild($elementEl = $xsd->createElementNS($xsdPackage->nsURI,'element'));
                        $elementEl->setAttribute('name',$attribute->name);
                        $elementEl->setAttribute('type',$ns[$attribute->type->package->nsURI].":".$attribute->type->name);
                        if(strlen($attribute->lowerBound)) $elementEl->setAttribute('minOccurs',is_numeric($attribute->lowerBound) ? $attribute->lowerBound : '0');
                        if(strlen($attribute->upperBound)) $elementEl->setAttribute('maxOccurs',is_numeric($attribute->upperBound) ? $attribute->upperBound : 'unbounded');
                        if($note = trim($attribute->note)) {
                            $elementEl->appendChild($annotationEl = $xsd->createElementNS($xsdPackage->nsURI,'annotation'));
                            $annotationEl->appendChild($documentationEl = $xsd->createElementNS($xsdPackage->nsURI,'documentation'));
                            $documentationEl->appendChild($xsd->createTextNode($note));
                        }
                    }
                }
            } else {
                //...
            }
            if($typeEl) {
                $schema->appendChild($elementEl = $xsd->createElementNS($xsdPackage->nsURI,'element'));
                $elementEl->setAttribute('name', $type->name);
                $elementEl->setAttribute('type',$ns[$type->package->nsURI].":".$type->name);
                if($note = trim($type->note)) {
                    $annotationEl = $typeEl->insertBefore($xsd->createElementNS($xsdPackage->nsURI, 'annotation'), $typeEl->firstChild);
                    $annotationEl->appendChild($documentationEl = $xsd->createElementNS($xsdPackage->nsURI, 'documentation'));
                    $documentationEl->appendChild($xsd->createTextNode($note));
                }
            }
        }

        $this->xsd = $xsd;
    }

    public function isNoWSGen() {
        for($package = $this;$package; $package = $package->package) if($package->stereotype == 'NoWSGen') return true;
        return false;
    }

    private static function htmlEscape($txt) {
        return Model::htmlEscape($txt);
    }
    public function typeURL(Type $type) {
        return $type->type == 'Class' || $type->type == 'Enumeration' ? $this->getRelativePathTo($type->package).'#'.$type->name : 'javascript:void(0)';
    }

    public function printBreadCrumbsHTML(Package $relativeTo) {
        /** @var Package[] $packagePath */
        $packagePath = array();
        for($vcPackage = $this;$vcPackage && $vcPackage->stereotype != 'VC';$vcPackage = $vcPackage->package) $packagePath[] = $vcPackage;
        $packagePath[] = $vcPackage;
        $packagePath[] = $vcPackage->package;
        $packagePath = array_reverse($packagePath);
        ?>
        <ol class="breadcrumb">
        <?php foreach($packagePath as $package):?>
            <li><a href="<?=$relativeTo->getRelativePathTo($package)?>"><?=self::htmlEscape($package->name)?></a></li>
        <?php endforeach;?>
        </ol>
        <?php
    }

    /**
     * @param Package[] $packages
     */
    public function printServicesNavigation($packages) { ?>
        <ul>
            <?php foreach ($packages as $package): ?>
                <?php if($package->stereotype) continue; ?>
                <li>
                    <a href="<?= $this->getRelativePathTo($package) ?>"><?= $package->name ?></a>
                    <?php if($package->types): ?>
                        <?php foreach ($package->types as $type): ?>
                            <?php if($type->type !== 'Interface' || !$type->operations) continue; ?>
                            <ul>
                                <li>
                                    <a href="<?= $this->getRelativePathTo($package) ?>#<?= $type->name ?>"><?= $type->name ?></a>
                                    <ul>
                                        <?php foreach ($type->operations as $operation): ?>
                                            <li class="scope-Public ">
                                                <a href="<?= $this->getRelativePathTo($package) ?>#<?= $type->name ?>-<?= $operation->name ?>" class="navigation_link" title="<?= self::htmlEscape($operation->name) ?>"><?= $operation->name ?></a>
                                            </li>
                                        <?php endforeach; ?>
                                    </ul>
                                </li>
                            </ul>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    <?php if($package->packages) $this->printServicesNavigation($package->packages); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php }

    public function createHTML() {
        for($vcPackage = $this;$vcPackage && $vcPackage->stereotype != 'VC';$vcPackage = $vcPackage->package);
        if(!$vcPackage) {
            /** @var Package $lastChild */
            $lastChild = end($this->packages);
            if($lastChild->stereotype == 'VC') {
                $this->html = new \DOMDocument();
                $this->html->loadHTML('
                    <!DOCTYPE html><html><head>
                       <meta http-equiv="refresh" content="0; url=./'.$lastChild->name.'/">
                    </head><body></body></html>
                ');
            }
            return;
        }

        $appPackage = $vcPackage->package;

        /** @var Type[] $types */
        $types = array();
        /** @var Type[] $services */
        $services = array();
        foreach($this->types as $type) {
            if(!empty($type->attributes) && !$type->isR()) $types[] = $type;
            if(!empty($type->operations)) $services[] = $type;
        }

        $subpackages = array();
        foreach($this->packages as $subpackage) {
            if(!$subpackage->isR()) $subpackages[] = $subpackage;
        }

        ob_start();
        ?><!DOCTYPE html>
        <html>
            <head>
                <meta http-equiv="content-type" content="text/html; charset=UTF-8">
                <title><?=self::htmlEscape($this->name.' ('.$appPackage->name.')')?></title>
                <script src="//ajax.googleapis.com/ajax/libs/jquery/3.0.0/jquery.min.js" integrity="sha384-THPy051/pYDQGanwU6poAc/hOdQxjnOEXzbT+OuUAFqNqFjL+4IGLBgCJC3ZOShY" crossorigin="anonymous"></script>

                <!-- Latest compiled and minified CSS -->
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">

                <!-- Optional theme -->
                <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap-theme.min.css">

                <!-- Latest compiled and minified JavaScript -->
                <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>

                <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.css" />
                <script src="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.1.1/ekko-lightbox.min.js"></script>

                <!--suppress CssUnusedSymbol -->
                <style>
                    h1,h2,h3,h4,h5,h6 {
                        color:rgb(83, 83, 155);
                    }
                    table.table {
                        margin-bottom: 40px;
                    }
                    .method-sub-title {
                        margin: 10px 0 0 0;
                        font-style: italic;
                        font-weight: bold;
                    }
                    @media print {
                        a[href]:after {
                            content: none !important;
                        }
                    }
                    .note {

                    }
                    img {
                        max-width:100%;
                    }



                </style>
                <script>
                    $(function() {
                        $('img').each(function(){
                            $(this).wrap($('<a href="'+$(this).attr('src')+'" data-toggle="lightbox"><a>'));
                        });

                        $(document).on('click', '[data-toggle="lightbox"]', function(event) {
                            event.preventDefault();
                            $(this).ekkoLightbox();
                        });
                    });
                </script>
            </head>
            <body style="">
            <div class="container">
                <div class="row">
                    <?php if(@$this->model->rootPackage): ?>
                        <div class="col-xs-0 col-sm-4 hidden-print">

                        </div>
                    <?php endif; ?>
                    <div class="col-xs-0 col-sm-4 hidden-print">
                    <?php if($vcPackage->nsURI == 'http://ee.econt.com/services'):?>
                        <h4 style="font-style: italic;margin-top: 20px;"><a href="<?=$this->getRelativePathTo($vcPackage) ?>"><?= self::htmlEscape($vcPackage->name) ?></a>:</h4>
                        <?php $this->printServicesNavigation($vcPackage->packages); ?>
                    <?php else:?>
                        <?php $this->model->printVCPackageList($this);?>
                    <?php endif;?>
                    </div>
                    <div class="col-xs-12 col-sm-8">
                        <h4><?php $this->printBreadCrumbsHTML($this)?></h4>
                        <p class="text-muted"><small><?=self::htmlEscape($this->nsURI)?></small><br/></p>
                        <div class="note"><?=($this->note)?></div>
                        <?php if(!empty($subpackages)):?>
                            <h4 style="font-style: italic;margin-top: 20px;">Subpackages:</h4>
                            <ul class="">
                                <?php foreach($subpackages as $package):?>
                                    <li><a href="<?=$this->getRelativePathTo($package)?>"><?=self::htmlEscape($package->name)?></a></li>
                                <?php endforeach;?>
                            </ul>
                        <?php endif;?>
                        <?php if(!empty($types)):?>
                            <h4 style="font-style: italic;margin-top: 40px;">Data types:</h4>
                            <?php foreach($types as $type){
                                 $type->printAttributesHTML($this);
                            }?>
                        <?php endif;?>
                        <?php if(!empty($services)):?>
                            <h4 style="font-style: italic;margin-top: 40px;">Services:</h4>
                        <?php endif;?>
                        <?php foreach($services as $service){
                            $service->printOperationsHTML($this);
                        }?>
                    </div>
                </div>
            </div>
            </body>
        </html><?php
        $this->html = new \DOMDocument();
        $this->html->loadHTML($s = ob_get_clean());
        $this->html->formatOutput = true;
    }
}