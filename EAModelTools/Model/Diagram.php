<?php
namespace EAModelTools\Model;

class Diagram {
    public $id;
    public $name;
    public $note;
    public $stereotype;
    public $type;
    /**
     * @var Package
     */
    public $package;

    /** @var Type[] */
    public $types = array();

    /** @var \DOMDocument */
    public $html;

    public function __construct($dbRow, Package $package = null) {
        $this->id = $dbRow['Diagram_ID'];
        $this->name = $dbRow['Name'];
        $this->note = Model::processNote($dbRow['Notes']);
        $this->stereotype = $dbRow['Stereotype'];
        $this->package = $package;
    }

    public function createHTML() {

        /** @var Type[][] $typesByVC */
        $typesByVC = array();
        foreach($this->types as $type) {
            for($vcPackage = $type->package;$vcPackage && $vcPackage->stereotype != 'VC';$vcPackage = $vcPackage->package);
            if(!$vcPackage) continue;
            $typesByVC[$vcPackage->package->name][] = $type;
        }

        ob_start();
        ?><!DOCTYPE html>
        <html>
        <head>
            <meta http-equiv="content-type" content="text/html; charset=UTF-8">
            <title><?=Model::htmlEscape($this->name)?></title>
            <style>
                h1,h2,h3,h4,h5,h6 {
                    color:rgb(83, 83, 155);
                }
                table.table {
                    margin-bottom: 40px;
                }
                .method-sub-title {
                    margin: 10px 0 0 0;
                    font-style: italic;
                    font-weight: bold;
                }
                @media print {
                    a[href]:after {
                        content: none !important;
                    }
                }
                span.note {
                    white-space: pre-wrap;
                }
            </style>
        </head>
        <body style="">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <h1 style="font-style: italic;margin-top: 40px;">Data types:</h1>
                    <?php foreach($typesByVC as $VCName => $types): $types = array_filter($types,function(Type $type){return !$type->isR() && !empty($type->attributes);})?>
                        <?php if(!empty($types)):?><h2><?=Model::htmlEscape($VCName)?></h2><?php endif;?>
                        <?php foreach($types as $type) $type->printAttributesHTML($this->package);
                    endforeach;?>
                    <h1 style="font-style: italic;margin-top: 40px;">Services:</h1>
                    <?php foreach($typesByVC as $VCName => $types): $types = array_filter($types,function(Type $type){return !$type->isR() && !empty($type->operations);})?>
                        <?php if(!empty($types)):?><h2><?=Model::htmlEscape($VCName)?></h2><?php endif;?>
                        <?php foreach($types as $type) $type->printOperationsHTML($this->package);
                    endforeach;?>
                </div>
            </div>
        </div>
        <p class="generated-at"><small>generated at <?=date("Y-m-d H:i")?></small></p>
        </body>
        </html><?php
        $this->html = new \DOMDocument();
        $this->html->loadHTML(ob_get_clean());
        $this->html->formatOutput = true;
    }
}