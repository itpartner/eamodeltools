<?php
namespace EAModelTools\DBC;

class DBC {
    /**
     * @var DBConnection
     */
    public static $izprati = null;

    /**
     * @var DBConnection
     */
    public static $log = null;

    /**
     * @var DBConnection[]
     */
    public static $connections = array();

    public static function SQLDateToTimeStamp($sqlDate) {
        if (is_numeric($sqlDate)) return $sqlDate;

        if (!empty($sqlDate) && !in_array($sqlDate, array('0000-00-00 00:00:00', '0000-00-00'))) {
            if (($nRes = strtotime($sqlDate)) != -1)
                return $nRes;
        }

        return 0;
    }

    public static function arrayAssocify(&$data, $depth) {
        if(empty($data) || $depth <= 0) return $data;
        $firstKey = reset(array_keys(reset($data)));
        if(empty($firstKey)) return $data;
        $result = array();
        foreach($data as $row) {
            $sKey = $row[$firstKey];
            unset($row[$firstKey]);
            if($depth > 1) {
                if(!array_key_exists($sKey,$result)) $result[$sKey] = array();
                $result[$sKey][] = $row;
            } else {
                $result[$sKey] = $row;
            }
        }
        if($depth > 1) foreach($result as $k => $row) {
            $result[$k] = self::arrayAssocify($row,$depth - 1);
        }
        return $result;
    }

    public static function stripslashes($data) {
        return is_array($data) ? array_map(__METHOD__, $data) : stripslashes($data);
    }

    public static function getTempDir() {
        $dir = sys_get_temp_dir();
        if(substr($dir,-1,1) != '\\' && substr($dir,-1,1) != '/') {
            $dir.='/';
        }
        return $dir;
    }
} 