<?php
namespace EAModelTools\DBC;

class DBTransaction {
    /**
     * @var DBConnection
     */
    private $dbConnection = null;

    public function __construct($dbConnection) {
        $this->dbConnection = $dbConnection;
    }
    public function complete() {
        $this->dbConnection->_completeTrans();
        $this->dbConnection = null;
    }

    public function rollback() {
        $this->dbConnection->_rollbackTrans();
        $this->dbConnection = null;
    }
} 