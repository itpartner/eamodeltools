<?php
namespace EAModelTools\DBC;

class DBConnection {
    /**
     * @var \mysqli
     */
    private $link = null;

    private $DSN;

    private $DBName = null;

    private $timeZoneOffset = '';

    /**
     * @var DBSQLException
     */
    private $connError = null;

    private $failedTrans = false;

    private $transCount = 0;

    /**
     * @var \Memcache
     */
    private $memcache = null;

    public function __construct($DSNURI, \Memcache $memcache = null)
    {
        $this->DSN = parse_url($DSNURI);
        $this->DBName = basename($this->DSN['path']);
        $this->memcache = $memcache;
    }

    private function checkThrow($sQuery = null) {
        if(!$this->link) throw new DBException("Not connected.");
        if($this->link->errno) throw new DBSQLException($sQuery,$this->link->errno,$this->link->error);
        if($this->link->connect_errno) throw new DBSQLException($sQuery,$this->link->connect_errno,$this->link->connect_error);
    }

    private function connect() {
        if($this->link) return;
        if($this->connError) throw $this->connError;
        try {
            if(!empty($this->DSN)) {

                $this->link = mysqli_connect($this->DSN['host'],$this->DSN['user'],$this->DSN['pass'],$this->DBName,$this->DSN['port']);
                $this->checkThrow();

                $this->link->set_charset('utf8');
                $this->checkThrow();
                $this->_Execute("SET NAMES utf8");

                $this->_Execute("SET SESSION group_concat_max_len = 400000");
                $this->_Execute("SET SESSION innodb_table_locks = FALSE");

                $this->resetTrans();

                if(!empty($this->timeZoneOffset)) $this->setTimeZone($this->timeZoneOffset);

            } else {
                throw new DBException('DSN is empty.');
            }
        } catch (DBException $e) {
            $this->connError = $e;
            throw $this->connError;
        }
    }

    public function getHost() {
        return $this->DSN['host'];
    }

    private function _Execute($query) {
        $res = $this->link->query($query);
        $this->checkThrow($query);
        return $res;
    }

    public function resetTrans() {
        $this->_failTrans();
        while($this->transCount) $this->_completeTrans();
    }

    public function isConnected() {
        return !!$this->link;
    }

    public function setTimeZone($zoneOffset) {
        $this->timeZoneOffset = $zoneOffset;
        if($this->isConnected()) $this->_Execute("SET time_zone = ".$this->quote($this->timeZoneOffset));
    }

    public function select($query, $cacheSeconds = 0,$assocDepth = 0 ) {
        $result = null;

        if($cacheSeconds > 0) $result = $this->cacheGet(__METHOD__, $query);

        if($result===NULL || $cacheSeconds <= 0)
        {
            $this->connect();
            $rs = $this->_Execute($query);
            $result = array();
            while($row = $rs->fetch_assoc()) $result[] = $row;

            $result = DBC::arrayAssocify($result, $assocDepth);

            if($cacheSeconds > 0) $this->cacheSet(__METHOD__,$query,$result,$cacheSeconds);
        }

        return $result;
    }

    public function selectAssoc($query, $cacheSeconds = 0) {
        $arrayResult = $this->select($query,$cacheSeconds);
        if(empty($arrayResult)) return $arrayResult;

        $result = array();
        $firstRowKeys = array_keys(reset($arrayResult));
        if(count($firstRowKeys) <= 2) {
            $keyField = $firstRowKeys[0];
            $valueField = count($firstRowKeys) == 2 ? $firstRowKeys[1] : $firstRowKeys[0];
            foreach($arrayResult as $row) $result[$row[$keyField]] = $row[$valueField];
        } else {
            $result = DBC::arrayAssocify($arrayResult, 1);
        }

        return $result;
    }

    public function selectRow($query, $cacheSeconds = 0) {
        $res = $this->select($query,$cacheSeconds);
        return empty($res) ? array() : reset($res);
    }

    public function selectColumn($query, $cacheSeconds = 0) {
        $queryResult = $this->select($query, $cacheSeconds);
        $result = array();
        foreach($queryResult as $aRow) $result[] = reset($aRow);
        return $result;
    }

    public function selectField($query, $cacheSeconds = 0) {
        $aColumn = $this->selectColumn($query, $cacheSeconds);
        return empty($aColumn) ? null : reset($aColumn);
    }

    public function affectedRows() {
        $this->connect();
        return $this->link->affected_rows;
    }

    public function lastInsertID() {
        return $this->isConnected() ? $this->link->insert_id : null;
    }

    public function multiInsert($tableName, $data, $mode = 'update', $updateFlag = true) {
        if(empty($data)) return 0;
        $fields = $this->getTableFields($tableName);

        $dataByColumns = array();

        //ako update-vame redovete se razdelqt na otdelni query-ta v zavisimost koi poleta gi ima
        if($mode == 'update') {
            foreach($data as $row) {
                $columnNames = array();
                foreach($row as $colName => $mVal) {
                    $colNameLower = strtolower($colName);
                    if(empty($fields[$colNameLower]) || ($mVal === null && !$fields[$colNameLower]['allow_null'])) continue;
                    $columnNames[] = $colNameLower;
                }
                sort($columnNames);
                $columnNamesKey = implode(':',$columnNames);
                if(!array_key_exists($columnNamesKey, $dataByColumns)) $dataByColumns[$columnNamesKey] = array();
                $dataByColumns[$columnNamesKey][] = $row;
            }
        } else {
            $dataByColumns['all'] = $data;
        }

        $affectedRows = 0;
        foreach($dataByColumns as $data) {
            if($updateFlag) {
                $sTime = date('Y-m-d H:i:s');
                #$nUser = defined('IS_ROBOT') && IS_ROBOT ? 1 : (int) $_SESSION['userdata']['id'];
                foreach($data as $k => $row) {
                    $data[$k]['created_time'] = $data[$k]['updated_time'] = $sTime;
                    #$data[$k]['created_user'] = $data[$k]['updated_user'] = $nUser;
                }
            }
            $insertFieldNamesQuoted = array();
            $rowsQuoted = array();
            foreach($data as $row) {
                $rowValuesQuoted = array();
                foreach($row as $fieldName => $value) {
                    $fieldNameLower = strtolower($fieldName);
                    if(empty($fields[$fieldNameLower])) continue;
                    if(empty($insertFieldNamesQuoted[$fieldNameLower])) $insertFieldNamesQuoted[$fieldNameLower] = '`'.$fields[$fieldNameLower]['name'].'`';
                    if($value === NULL) {
                        $rowValuesQuoted[$fieldNameLower] = NULL;
                    } else {
                        if(!is_scalar($value)) continue;
                        switch($fields[$fieldNameLower]['type']) {
                            case 'tinyint':
                            case 'smallint':
                            case 'mediumint':
                            case 'bigint':
                            case 'int':
                            case 'float':
                            case 'double':
                            case 'decimal':
                                if(is_numeric($value)) $rowValuesQuoted[$fieldNameLower] = $value;
                                else $rowValuesQuoted[$fieldNameLower] = $this->quote($value);
                                break;

                            case 'set':
                            case 'enum':
                            case 'char':
                            case 'varchar':
                            case 'tinytext':
                            case 'text':
                            case 'mediumtext':
                            case 'longtext':
                            case 'blob':
                            case 'year':
                                $rowValuesQuoted[$fieldNameLower] = $this->quote($value);
                                break;

                            case 'date':
                                $rowValuesQuoted[$fieldNameLower] = $this->quote( is_numeric($value) ? date("Y-m-d",$value) : $value);
                                break;

                            case 'time':
                                $rowValuesQuoted[$fieldNameLower] = $this->quote( is_numeric($value) ? date("H:i:s",$value) : $value);
                                break;

                            case 'datetime':
                            case 'timestamp':
                                $rowValuesQuoted[$fieldNameLower] = $this->quote( is_numeric($value) ? date("Y-m-d H:i:s",$value) : $value);
                                break;

                            default: throw new DBException(sprintf("Unknown field type (%s).", $fields[$fieldNameLower]['type']));
                        }
                    }
                }
                $rowsQuoted[] = $rowValuesQuoted;
            }
            $SQLRows = array();
            foreach($rowsQuoted as $rowQuoted) {
                if(empty($rowQuoted)) continue;
                $tmpRow = array();
                foreach($insertFieldNamesQuoted as $fieldNameLower => $SQLFieldName) {
                    if(!array_key_exists($fieldNameLower, $rowQuoted)) {
                        $tmpRow[] = 'default';
                    }
                    elseif($rowQuoted[$fieldNameLower] === NULL) {
                        $tmpRow[] = $fields[$fieldNameLower]['allow_null'] ? 'null' : 'default';
                    } else {
                        $tmpRow[] = $rowQuoted[$fieldNameLower];
                    }
                }
                if(empty($tmpRow)) continue;
                $SQLRows[] = '('.implode(',',$tmpRow).')';
            }

            if(empty($SQLRows)) return 0;

            if($mode == 'replace')  $query = "REPLACE INTO";
            elseif($mode == 'ignore') $query = "INSERT IGNORE INTO";
            elseif($mode == 'delayed') $query = "INSERT DELAYED INTO";
            else $query = "INSERT INTO";

            $query .= " ".$tableName." ";
            $query .= " (".implode(',',$insertFieldNamesQuoted).") VALUES \n";
            $query .= implode(",\n",$SQLRows) ."\n";

            if($mode == 'update') {
                $updateFields = array();
                foreach($insertFieldNamesQuoted as $fieldNameLower => $fieldName) {
                    if($updateFlag && ($fieldNameLower == 'created_time' || $fieldNameLower == 'created_user')) continue;
                    $updateFields[] = "$fieldName = VALUES($fieldName)";
                }
                $query .= "ON DUPLICATE KEY UPDATE ".implode(',', $updateFields)." \n";
            }

            $this->execute($query);
            $affectedRows += $this->affectedRows();
        }
        return $affectedRows;
    }

    private $tableFieldsCache = array();
    public function getTableFields($tableName) {
        $this->connect();
        if(!preg_match("/^[a-zA-Z\\d_\\.]+$/", $tableName)) throw new DBException("Invalid table name.");
        if(empty($this->tableFieldsCache[$tableName])) {
            $cacheKey = $this->getHost().'-'.$tableName;
            $fields = $this->cacheGet(__METHOD__,$cacheKey);
            if(!$fields) {
                $SQLFields = $this->select("SHOW FIELDS FROM $tableName");
                $fields = array();
                foreach($SQLFields as $SQLField) {
                    $field = array();
                    $matches = array();
                    preg_match("/^([a-zA-Z]+).*/", $SQLField['Type'], $matches);
                    $field['type'] = strtolower($matches[1]);
                    $field['allow_null'] = $SQLField['Null'] == 'YES';
                    $field['has_default'] = $SQLField['Default'] === NULL;
                    $field['name'] = $SQLField['Field'];
                    $fields[strtolower($field['name'])] = $field;
                }
                $this->cacheSet(__METHOD__,$cacheKey,$fields,600);
            }
            $this->tableFieldsCache[$tableName] = $fields;
        }

        return $this->tableFieldsCache[$tableName];
    }

    private function addDebugInfoToQuery($sQuerey) {
        if(defined('DEBUG_BACKTRACE_IGNORE_ARGS'))
            foreach(array_slice(@debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS),1) as $aTraceItem) {
                if(!empty($aTraceItem['file'])) {
                    $sQuerey.="#".preg_replace("/[^a-zA-Z0-9.\\-_\\/\\\\]/",'',$aTraceItem['file']).':'.$aTraceItem['line']."\n";
                }
            }

        return $sQuerey;
    }

    public function escape($sValue) {
        $this->connect();
        return $this->link->real_escape_string($sValue);
    }

    public function quote($sValue) {
        return "'".$this->escape($sValue)."'";
    }

    /**
     * @param string $query
     */
    public function execute($query) {
        $this->connect();
        $query = $this->addDebugInfoToQuery($query);
        $this->_Execute($query);
    }

    public function startTrans() {
        $this->transCount++;
        try {
            if($this->transCount == 1) {
                $this->execute("SET AUTOCOMMIT=0");
                $this->execute("BEGIN");
            }
            $this->execute("SAVEPOINT t".($this->transCount));
        } catch (\Exception $e) {
            $this->failedTrans = true;
            throw $e;
        }
        return new DBTransaction($this);
    }

    public function _completeTrans() {
        if(!$this->transCount) return;
        $this->transCount--;
        if(!$this->transCount) {
            if($this->failedTrans) {
                unset($this->cache[1]);//tunkajno zabursvame cache-a predi da se e zapisal
                $this->execute("ROLLBACK");
            } else {
                $this->execute("COMMIT");
            }
            $this->execute("SET AUTOCOMMIT = 1");
            $this->failedTrans = false;
        }
        $this->mergeCache($this->transCount);
    }

    /**
     * merge-va cache-a s 1 nivo ot tranzakciqta. ako nivoto e 0 znachi commit-vame i go zapisva v memcache
     * @param $nTransCount
     */
    private function mergeCache($nTransCount) {
        if($this->cache[$nTransCount + 1]) foreach($this->cache[$nTransCount + 1] as $sKey => $aVal) {//cache schemi
            $this->cache[$nTransCount][$sKey] = $aVal;
            if(!$nTransCount && $this->memcache) $this->memcache->set($sKey,$aVal,null,$aVal['expireSeconds']);
        }
        unset($this->cache[$nTransCount + 1]);
    }

    public function _failTrans() {
        if($this->transCount) $this->failedTrans = true;
    }

    public function _rollbackTrans() {
        if(!$this->transCount) return;
        if($this->transCount == 1) {
            $this->failedTrans = true;
            $this->_completeTrans();
        } else {
            unset($this->cache[$this->transCount]);//cache schemi
            $this->transCount--;
            try {
                $this->execute("ROLLBACK TO SAVEPOINT t".($this->transCount + 1));
                $this->execute("RELEASE SAVEPOINT t".($this->transCount + 1));
            } catch (\Exception $e) {
                $this->failedTrans = true;
                throw $e;
            }
        }
        return;
    }

    //cache-a e tuka zashtoto e svurzan s tranzakciite. inache nqma nishto obashto s DB
    /**
     * cache[0] e commit-natiq cache 1,2,3... e po savepoint-i
     * @var array
     */
    private $cache = array();
    public function cacheSet($sName, $mKey, $mValue, $nExpire = NULL) {
        $sKey = $sName.':'.self::cacheGetHash($mKey);
        if($nExpire!==NULL) {
            $nExpire = (int) $nExpire;
        }
        if($nExpire > 0) {
            $aVal = array('value' => $mValue, 'time' => time(), 'expireSeconds' => $nExpire, 'expireTime' => time()+$nExpire);
            $this->cache[$this->transCount][$sKey] = $aVal;
            if(!$this->transCount && $this->memcache) $this->memcache->set($sKey,$aVal,null,$aVal['expireSeconds']);//nqma tranzakciq i se zapisva directno
        }
    }
    public function cacheGet($sName, $mKey) {
        $sKey = $sName.':'.self::cacheGetHash($mKey);

        $aVal = null;
        $i = empty($this->cache) ? 0 : max(array_keys($this->cache));
        while($i --> 0 && !$aVal) if($this->cache[$i] && array_key_exists($sKey,$this->cache[$i])) $aVal = $this->cache[$i][$sKey];//izdirvame localno
        if(!$aVal) $this->cache[0][$sKey] = $aVal = $this->memcache ? $this->memcache->get($sKey) : null;//ako go nqma izdirvame v memcache

        if($aVal['expireTime']<time()) return null;
        return $aVal['value'];
    }
    public static function cacheGetHash() {
        return md5(serialize(self::cacheGetHashArray(func_get_args())));
    }
    private static function cacheGetHashArray($var) {
        if(is_object($var)) $var = (array) $var;
        if(is_array($var)) foreach ($var as $k => $v) {
            if($v === null) continue;
            else if(is_scalar($v)) $var[$k] = (string) $v;
            else $var[$k] = self::cacheGetHashArray($v);
        }
        ksort($var);
        return $var;
    }
}