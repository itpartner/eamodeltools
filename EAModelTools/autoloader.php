<?php
spl_autoload_register(function($name){
    if(!preg_match("/^EAModelTools\\\\([a-zA-Z\\d\\\\]+)$/",$name,$matches)) return;
    include dirname(__FILE__).'/'.str_replace('\\','/',$matches[1]).'.php';
});